EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Single Chip Connector to DisplayPort"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L connsingle:ConnSingle J_conn1
U 1 1 5BE2176A
P 2150 3450
F 0 "J_conn1" H 2250 5850 60  0000 C CNN
F 1 "ConnSingle" H 2200 2250 60  0000 C CNN
F 2 "Library:connector" H 2150 2950 60  0001 C CNN
F 3 "" H 2150 2950 60  0001 C CNN
	1    2150 3450
	1    0    0    -1  
$EndComp
$Comp
L display_port:DISPLAY_PORT J1
U 1 1 5BE217B8
P 2500 6050
F 0 "J1" H 1900 7150 60  0000 C CNN
F 1 "DISPLAY_PORT" V 2650 6050 60  0000 C CNN
F 2 "Library:DP1RD20JQ1R400" H 2450 6050 60  0001 C CNN
F 3 "" H 2450 6050 60  0000 C CNN
	1    2500 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5BE21828
P 9500 1500
F 0 "C1" H 9525 1600 50  0000 L CNN
F 1 "100n" H 9525 1400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 9538 1350 50  0001 C CNN
F 3 "" H 9500 1500 50  0001 C CNN
	1    9500 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5BE21869
P 9850 1500
F 0 "C2" H 9875 1600 50  0000 L CNN
F 1 "100n" H 9875 1400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 9888 1350 50  0001 C CNN
F 3 "" H 9850 1500 50  0001 C CNN
	1    9850 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5BE218B0
P 9600 2100
F 0 "#PWR01" H 9600 1850 50  0001 C CNN
F 1 "GND" H 9600 1950 50  0000 C CNN
F 2 "" H 9600 2100 50  0001 C CNN
F 3 "" H 9600 2100 50  0001 C CNN
	1    9600 2100
	1    0    0    -1  
$EndComp
NoConn ~ 1650 6850
NoConn ~ 1650 6950
NoConn ~ 1650 7050
Wire Wire Line
	1650 5150 1150 5150
Wire Wire Line
	1650 5250 1150 5250
Wire Wire Line
	1650 5350 1150 5350
Wire Wire Line
	1650 5450 1150 5450
Wire Wire Line
	1650 5550 1150 5550
Wire Wire Line
	1650 5650 1150 5650
Wire Wire Line
	1650 5750 1150 5750
Wire Wire Line
	1650 5850 1150 5850
Wire Wire Line
	1650 5950 1150 5950
Wire Wire Line
	1650 6050 1150 6050
Wire Wire Line
	1650 6150 1150 6150
Wire Wire Line
	1650 6250 1150 6250
Wire Wire Line
	1650 6350 1150 6350
Wire Wire Line
	1650 6450 1150 6450
Wire Wire Line
	1650 6550 1150 6550
Wire Wire Line
	1650 6650 1150 6650
Wire Wire Line
	1650 6750 1150 6750
Text Label 1150 5150 0    60   ~ 0
FE_DO_0_N
Text Label 1150 5250 0    60   ~ 0
FGND
Text Label 1150 5350 0    60   ~ 0
FE_DO_0_P
Text Label 1150 5450 0    60   ~ 0
FE_DO_1_N
Text Label 1150 5650 0    60   ~ 0
FE_DO_1_P
Text Label 1150 5750 0    60   ~ 0
FE_DO_2_N
Text Label 1150 5950 0    60   ~ 0
FE_DO_2_P
Text Label 1150 6050 0    60   ~ 0
FE_DO_3_N
Text Label 1150 6250 0    60   ~ 0
FE_DO_3_P
Text Label 1150 5550 0    60   ~ 0
FGND
Text Label 1150 5850 0    60   ~ 0
FGND
Text Label 1150 6150 0    60   ~ 0
FGND
Text Label 1150 6350 0    60   ~ 0
FE_NTC
Text Label 1150 6450 0    60   ~ 0
FE_NTC_RET
Text Label 1150 6550 0    60   ~ 0
FE_CMD_N
Text Label 1150 6750 0    60   ~ 0
FE_CMD_P
Text Label 1150 6650 0    60   ~ 0
FGND
Text Label 2300 7400 0    60   ~ 0
SGND
Text Label 9600 2100 0    60   ~ 0
GND
Text Label 9500 1650 3    60   ~ 0
GND
Text Label 9850 1650 3    60   ~ 0
GND
Text Label 9850 1350 0    60   ~ 0
SGND
Text Label 9500 1350 0    60   ~ 0
FGND
$Comp
L molex:molex M1
U 1 1 5BE216A4
P 10050 3050
F 0 "M1" H 10040 3220 60  0000 C CNN
F 1 "molex" H 10049 2885 60  0000 C CNN
F 2 "Library:molex" H 10350 3400 60  0001 C CNN
F 3 "" H 10350 3400 60  0001 C CNN
	1    10050 3050
	1    0    0    -1  
$EndComp
$Comp
L solder_pad:Solder_Pad U3
U 1 1 5BE2171C
P 10650 3700
F 0 "U3" H 10700 3850 59  0000 C CNN
F 1 "Solder_Pad" H 10700 3950 59  0000 C CNN
F 2 "Library:Power_Pad" H 11500 4100 59  0001 C CNN
F 3 "" H 11500 4100 59  0001 C CNN
	1    10650 3700
	1    0    0    -1  
$EndComp
$Comp
L solder_pad:Solder_Pad U4
U 1 1 5BE21773
P 10650 4100
F 0 "U4" H 10700 4250 59  0000 C CNN
F 1 "Solder_Pad" H 10700 4350 59  0000 C CNN
F 2 "Library:Power_Pad" H 11500 4500 59  0001 C CNN
F 3 "" H 11500 4500 59  0001 C CNN
	1    10650 4100
	1    0    0    -1  
$EndComp
$Comp
L solder_pad:Solder_Pad U1
U 1 1 5BE217E2
P 9650 3700
F 0 "U1" H 9700 3850 59  0000 C CNN
F 1 "Solder_Pad" H 9700 3950 59  0000 C CNN
F 2 "Library:Power_Pad" H 10500 4100 59  0001 C CNN
F 3 "" H 10500 4100 59  0001 C CNN
	1    9650 3700
	-1   0    0    1   
$EndComp
$Comp
L solder_pad:Solder_Pad U2
U 1 1 5BE2188B
P 9650 4100
F 0 "U2" H 9700 4250 59  0000 C CNN
F 1 "Solder_Pad" H 9700 4350 59  0000 C CNN
F 2 "Library:Power_Pad" H 10500 4500 59  0001 C CNN
F 3 "" H 10500 4500 59  0001 C CNN
	1    9650 4100
	-1   0    0    1   
$EndComp
Text Label 10350 3000 0    60   ~ 0
GND
Text Label 10350 3100 0    60   ~ 0
GND
Text Label 9750 3000 2    60   ~ 0
VIN_RAW
Text Label 9750 3100 2    60   ~ 0
VIN_RAW
Text Label 10450 3700 2    60   ~ 0
GND
Text Label 10450 4100 2    60   ~ 0
GND
Text Label 9850 3700 0    60   ~ 0
VIN_RAW
Text Label 9850 4100 0    60   ~ 0
VIN_RAW
$Comp
L Connector1:LEMO2 J2
U 1 1 5BE21E4B
P 10050 5200
F 0 "J2" H 10050 5450 50  0000 C CNN
F 1 "LEMO2" H 10050 5050 50  0000 C CNN
F 2 "Library:lemo_epb00250ntn" H 10050 5250 50  0001 C CNN
F 3 "" H 10050 5250 50  0001 C CNN
	1    10050 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 5100 9150 5100
Text Label 9150 5100 0    60   ~ 0
FE_HV
Wire Wire Line
	9750 5200 9150 5200
Text Label 9150 5200 0    60   ~ 0
FE_HV_RET
$Comp
L Connector_Generic:Conn_02x01 J3
U 1 1 5CF7E8E2
P 5300 1300
F 0 "J3" H 5350 1517 50  0000 C CNN
F 1 "Conn_02x01" H 5350 1426 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 1300 50  0001 C CNN
F 3 "~" H 5300 1300 50  0001 C CNN
	1    5300 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  2850 1600 2850
Text Label 700  2850 0    50   ~ 0
FE_CMD_P
Text Label 3800 2750 2    50   ~ 0
FE_CMD_N
Wire Wire Line
	5600 1300 6500 1300
Text Label 6500 1300 2    50   ~ 0
FE_CMD_N
Wire Wire Line
	4200 1300 5100 1300
Text Label 4200 1300 0    50   ~ 0
FE_CMD_P
Wire Wire Line
	2900 2350 3800 2350
Text Label 3800 2350 2    50   ~ 0
FE_DO_1_N
Wire Wire Line
	700  2250 1600 2250
Text Label 700  2250 0    50   ~ 0
FE_DO_1_P
Wire Wire Line
	2900 1750 3800 1750
Text Label 3800 1750 2    50   ~ 0
FE_DO_0_P
Wire Wire Line
	700  1650 1600 1650
Text Label 700  1650 0    50   ~ 0
FE_DO_0_N
Wire Wire Line
	2900 3150 3800 3150
Text Label 3800 3150 2    50   ~ 0
FE_DO_3_N
Wire Wire Line
	2900 4150 3800 4150
Text Label 3800 4150 2    50   ~ 0
FE_DO_2_P
Wire Wire Line
	700  3250 1600 3250
Text Label 700  3250 0    50   ~ 0
FE_DO_3_P
Wire Wire Line
	700  4250 1600 4250
Text Label 700  4250 0    50   ~ 0
FE_DO_2_N
$Comp
L Connector_Generic:Conn_02x01 J4
U 1 1 5CF85927
P 5300 1600
F 0 "J4" H 5350 1817 50  0000 C CNN
F 1 "Conn_02x01" H 5350 1726 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 1600 50  0001 C CNN
F 3 "~" H 5300 1600 50  0001 C CNN
	1    5300 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 1600 6500 1600
Text Label 6500 1600 2    50   ~ 0
FE_DO_0_N
Wire Wire Line
	4200 1600 5100 1600
Text Label 4200 1600 0    50   ~ 0
FE_DO_0_P
$Comp
L Connector_Generic:Conn_02x01 J5
U 1 1 5CF8619E
P 5300 1900
F 0 "J5" H 5350 2117 50  0000 C CNN
F 1 "Conn_02x01" H 5350 2026 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 1900 50  0001 C CNN
F 3 "~" H 5300 1900 50  0001 C CNN
	1    5300 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 1900 6500 1900
Text Label 6500 1900 2    50   ~ 0
FE_DO_1_N
Wire Wire Line
	4200 1900 5100 1900
Text Label 4200 1900 0    50   ~ 0
FE_DO_1_P
$Comp
L Connector_Generic:Conn_02x01 J6
U 1 1 5CF86AF9
P 5300 2200
F 0 "J6" H 5350 2417 50  0000 C CNN
F 1 "Conn_02x01" H 5350 2326 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 2200 50  0001 C CNN
F 3 "~" H 5300 2200 50  0001 C CNN
	1    5300 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2200 6500 2200
Text Label 6500 2200 2    50   ~ 0
FE_DO_2_N
Wire Wire Line
	4200 2200 5100 2200
Text Label 4200 2200 0    50   ~ 0
FE_DO_2_P
$Comp
L Connector_Generic:Conn_02x01 J7
U 1 1 5CF8754B
P 5300 2500
F 0 "J7" H 5350 2717 50  0000 C CNN
F 1 "Conn_02x01" H 5350 2626 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 2500 50  0001 C CNN
F 3 "~" H 5300 2500 50  0001 C CNN
	1    5300 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2500 6500 2500
Text Label 6500 2500 2    50   ~ 0
FE_DO_3_N
Wire Wire Line
	4200 2500 5100 2500
Text Label 4200 2500 0    50   ~ 0
FE_DO_3_P
$Comp
L Connector_Generic:Conn_02x01 J8
U 1 1 5CF8881C
P 5300 2800
F 0 "J8" H 5350 3017 50  0000 C CNN
F 1 "Conn_02x01" H 5350 2926 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 2800 50  0001 C CNN
F 3 "~" H 5300 2800 50  0001 C CNN
	1    5300 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2800 6500 2800
Text Label 6500 2800 2    50   ~ 0
D12_N
Text Label 4200 2800 0    50   ~ 0
D12_P
$Comp
L Connector_Generic:Conn_02x01 J9
U 1 1 5CF89483
P 5300 3100
F 0 "J9" H 5350 3317 50  0000 C CNN
F 1 "Conn_02x01" H 5350 3226 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 3100 50  0001 C CNN
F 3 "~" H 5300 3100 50  0001 C CNN
	1    5300 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3100 6500 3100
Text Label 6500 3100 2    50   ~ 0
D13_N
Wire Wire Line
	4200 3100 5100 3100
Text Label 4200 3100 0    50   ~ 0
D13_P
$Comp
L Connector_Generic:Conn_02x01 J10
U 1 1 5CF8A223
P 5300 3400
F 0 "J10" H 5350 3617 50  0000 C CNN
F 1 "Conn_02x01" H 5350 3526 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 3400 50  0001 C CNN
F 3 "~" H 5300 3400 50  0001 C CNN
	1    5300 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3400 6500 3400
Text Label 6500 3400 2    50   ~ 0
D22_N
Wire Wire Line
	4200 3400 5100 3400
Text Label 4200 3400 0    50   ~ 0
D22_P
$Comp
L Connector_Generic:Conn_02x01 J11
U 1 1 5CF8A22D
P 5300 3700
F 0 "J11" H 5350 3917 50  0000 C CNN
F 1 "Conn_02x01" H 5350 3826 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 3700 50  0001 C CNN
F 3 "~" H 5300 3700 50  0001 C CNN
	1    5300 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3700 6500 3700
Text Label 6500 3700 2    50   ~ 0
D23_N
Wire Wire Line
	4200 3700 5100 3700
Text Label 4200 3700 0    50   ~ 0
D23_P
$Comp
L Connector_Generic:Conn_02x01 J12
U 1 1 5CF8B336
P 5300 4000
F 0 "J12" H 5350 4217 50  0000 C CNN
F 1 "Conn_02x01" H 5350 4126 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 4000 50  0001 C CNN
F 3 "~" H 5300 4000 50  0001 C CNN
	1    5300 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4000 6500 4000
Text Label 6500 4000 2    50   ~ 0
D32_N
Wire Wire Line
	4200 4000 5100 4000
Text Label 4200 4000 0    50   ~ 0
D32_P
$Comp
L Connector_Generic:Conn_02x01 J13
U 1 1 5CF8B340
P 5300 4300
F 0 "J13" H 5350 4517 50  0000 C CNN
F 1 "Conn_02x01" H 5350 4426 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 4300 50  0001 C CNN
F 3 "~" H 5300 4300 50  0001 C CNN
	1    5300 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4300 6500 4300
Text Label 6500 4300 2    50   ~ 0
D33_N
Wire Wire Line
	4200 4300 5100 4300
Text Label 4200 4300 0    50   ~ 0
D33_P
$Comp
L Connector_Generic:Conn_02x01 J14
U 1 1 5CF8B34A
P 5300 4600
F 0 "J14" H 5350 4817 50  0000 C CNN
F 1 "Conn_02x01" H 5350 4726 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 4600 50  0001 C CNN
F 3 "~" H 5300 4600 50  0001 C CNN
	1    5300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4600 6500 4600
Text Label 6500 4600 2    50   ~ 0
D42_N
Wire Wire Line
	4200 4600 5100 4600
Text Label 4200 4600 0    50   ~ 0
D42_P
$Comp
L Connector_Generic:Conn_02x01 J15
U 1 1 5CF8B354
P 5300 4900
F 0 "J15" H 5350 5117 50  0000 C CNN
F 1 "Conn_02x01" H 5350 5026 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 5300 4900 50  0001 C CNN
F 3 "~" H 5300 4900 50  0001 C CNN
	1    5300 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4900 6500 4900
Text Label 6500 4900 2    50   ~ 0
D43_N
Wire Wire Line
	4200 4900 5100 4900
Text Label 4200 4900 0    50   ~ 0
D43_P
Wire Wire Line
	4200 2800 5100 2800
Wire Wire Line
	2900 2750 3800 2750
Text Label 3800 2150 2    50   ~ 0
D12_P
Wire Wire Line
	3800 2550 2900 2550
Text Label 3800 2550 2    50   ~ 0
D13_P
Wire Wire Line
	700  1850 1600 1850
Text Label 700  1850 0    50   ~ 0
D22_P
Wire Wire Line
	700  1450 1600 1450
Text Label 700  1450 0    50   ~ 0
D23_P
Wire Wire Line
	3800 3750 2900 3750
Text Label 3800 3750 2    50   ~ 0
D32_P
Wire Wire Line
	3800 3350 2900 3350
Text Label 3800 3350 2    50   ~ 0
D33_P
Wire Wire Line
	700  3650 1600 3650
Text Label 700  3650 0    50   ~ 0
D42_P
Wire Wire Line
	700  4050 1600 4050
Text Label 700  4050 0    50   ~ 0
D43_P
Wire Wire Line
	3800 2150 2900 2150
Wire Wire Line
	1600 2050 700  2050
Text Label 700  2050 0    50   ~ 0
D12_N
Wire Wire Line
	1600 2450 700  2450
Text Label 700  2450 0    50   ~ 0
D13_N
Wire Wire Line
	2900 1950 3800 1950
Text Label 3800 1950 2    50   ~ 0
D22_N
Wire Wire Line
	2900 1550 3800 1550
Text Label 3800 1550 2    50   ~ 0
D23_N
Wire Wire Line
	1600 3850 700  3850
Text Label 700  3850 0    50   ~ 0
D32_N
Wire Wire Line
	1600 3450 700  3450
Text Label 700  3450 0    50   ~ 0
D33_N
Wire Wire Line
	2900 3550 3800 3550
Text Label 3800 3550 2    50   ~ 0
D42_N
Wire Wire Line
	2900 3950 3800 3950
Text Label 3800 3950 2    50   ~ 0
D43_N
$Comp
L Device:C C3
U 1 1 5CFF2908
P 7900 1600
F 0 "C3" V 7648 1600 50  0000 C CNN
F 1 "C" V 7739 1600 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 7938 1450 50  0001 C CNN
F 3 "~" H 7900 1600 50  0001 C CNN
	1    7900 1600
	0    1    1    0   
$EndComp
Text Label 8050 1600 0    50   ~ 0
GND
Text Label 7750 1600 2    50   ~ 0
C_GND
Text Label 2900 2950 0    50   ~ 0
C_GND
Text Label 2900 1350 0    50   ~ 0
C_GND
Text Label 2900 4350 0    50   ~ 0
C_GND
Text Label 1600 4450 2    50   ~ 0
C_GND
Text Label 1600 3050 2    50   ~ 0
C_GND
Text Label 1600 2650 2    50   ~ 0
C_GND
Text Label 1600 1250 2    50   ~ 0
C_GND
$EndSCHEMATC
